# This is a self-documenting Makefile, so everything after a double octothorpe
# will print out when you do that.
#
# - meaning, if you change stuff, document it with the double octothorpe/hash
#   signs plz

.PHONY: all docs help install

## Help for this makefile in `bps-dev`:
## 
help: Makefile
	@sed -n 's/^##//p' $<

## install		This just tells you what to install! Not installing it for you.
install:
	@echo "Go install `nextflow` and `singularity` to run the pipelines, and `python3`, and `tox` for making the docs."

## docs			This builds the sphinx docs, again using tox
docs:
	python3 -m tox -e build_docs

## call_bc		Run the barcode-barcode analysis
call_bc: main.nf nextflow.config nuconfig/config-bc.yaml
	nextflow $(word 1,$^) -c $(word 2,$^) \
		-resume -ansi-log false -with-dag reports/dag.html \
		-profile common,blocky \
		--slurm_queue med \
		-params-file $(word 3,$^)

## call_lasso	Run the LASSO analysis
call_lasso: main.nf nextflow.config nuconfig/config-lasso.yaml
	nextflow $(word 1,$^) -c $(word 2,$^) \
		-resume -ansi-log false -with-dag reports/dag.html \
		-profile common,blocky \
		--slurm_queue high \
		-params-file $(word 3,$^)


## call_1100	Run the 1100 random oligos analysis
call_1100: main.nf nextflow.config nuconfig/config-1100.yaml
	nextflow $(word 1,$^) -c $(word 2,$^) \
		-resume -ansi-log false -with-dag reports/dag.html \
		-profile common,blocky \
		--slurm_queue high \
		-params-file $(word 3,$^)



