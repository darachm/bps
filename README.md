# BPS

See the [docs here](https://darachm.gitlab.io/bps) for information about
how to run the analysis, or wetlab protocol.

Please use the [Issues](https://gitlab.com/darachm/bps/-/issues) page to
request support for application or modification of the 
wetbench or bioinformatics methods.

This takes FASTQ files 
to split up the reads based on barcodes, 
then builds a draft assembly and polishes it.
